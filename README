This is a SPANK plugin for Slurm that can create private temporary
directories for each job. This is useful for /tmp, /var/tmp and other
node-local temporary directories.

The implementation uses unshare(CLONE_NEWNS) to unshare the mount
namespace and then bind mounts the private directories.

If you have issues please let us know or fix it and send us a patch.

Author:

    Greg Wickham <greg.wickham@kaust.edu.sa>

Derived from work by:

    Author:
	    Magnus Jonsson <magnus@hpc2n.umu.se>

    Contributions from:
	    Lars Viklund <lars@hpc2n.umu.se>
	    Ake Sandgren <ake@hpc2n.umu.se>
	    Pär Lindfors <paran@nsc.liu.se>

Configuration parameters
------------------------

The SPANK plugin use the following parameters.

base:   For each job the plugin will create a directory named
        $base.$SLURM_JOB_ID.$SLURM_RESTART_COUNT

mount:  Private mount point. This can be specified more than once.

        For each mount, a directory will be created in the base dir
        and then bind mounted on the specified mount point.

tmpfs:  Creates a tmpfs mount point that uses memory from the
        job allocation.

        Maybe specified more than once

        The string '<uid>' is mapped to the users UID, so that
        for a user with uid 1234 a mount /run/user/1234
        can be created.

tmpfs/options:

        This is a raw string that is used when mounting tmpfs. 
        Use this to constrain a mount point to a certain size:
        (ie: size=10g to limit this tmpfs mount to 10GB)
        Refer to tmpfs(5) for more information

Example
-------

example plugstack.conf:

required  private-tmpdir.so  base=/local/tmp/ mount=/var/tmp mount=/tmp mount=/local/scratch tmpfs=/dev/shm tmpfs=/run/user/<uid> tmpfs/options=size=10g

When a job with jobid 100 and restart count 0, the following
directories will be created on compute nodes:

  /local/tmp/1133.0.0/
  /local/tmp/1133.0.0/var
  /local/tmp/1133.0.0/var/tmp
  /local/tmp/1133.0.0/tmp
  /local/tmp/1133.0.0/local
  /local/tmp/1133.0.0/local/scratch

In private namespaces the following bind mounts are done:

  /local/tmp/1133.0.0/var/tmp   on /var/tmp
  /local/tmp/1133.0.0/tmp on /tmp
  /local/tmp/1133.0.0/local on /local/scratch

And finally:

  /dev/shm -> tmpfs
  /run/user/XXXXX -> tmpfs

Note: It is important that mount=/tmp is specified
      last, since after /tmp/slurm.100.0/tmp is bind mounted on /tmp
      directories in /tmp/slurm.* can no longer be accessed.

Cleanup
-------

The plugin cleans up at the conclusion of the job.

A brief summary of the cleanup work will appear in the slurmd log

