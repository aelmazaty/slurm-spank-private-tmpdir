/*
 * Spank plugin private-tmpdir (c) HPC2N.umu.se
 * Author: Magnus Jonsson <magnus@hpc2n.umu.se>
 * Author: Lars Viklund <lars@hpc2n.umu.se>
 * Author: Ake Sandgren <ake@hpc2n.umu.se>
 * Author: Pär Lindfors <paran@nsc.liu.se>
 * Author: Greg Wickham <greg.wickham@kaust.edu.sa>
 */

/* Needs to be defined before first invocation of features.h so enable
 * it early. */
#define _GNU_SOURCE /* See feature_test_macros(7) */

#undef DEBUG

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <sys/time.h>
#include <sys/mount.h>
#include <slurm/spank.h>
#include <unistd.h>
#include <sched.h>
#include <ftw.h>
#include <errno.h>
#include <stdarg.h>

#include    <slurm/slurm.h>

SPANK_PLUGIN(private-tmpdir, 1);

#define MODULE_NAME "private-tmpdir"

// Default
#define MAX_BIND_DIRS 16

// Globals
static uid_t uid = (uid_t) - 1;
static gid_t gid = (gid_t) - 1;
static uint32_t jobid;
static uint32_t stepid;
static uint32_t restartcount;

typedef enum {
    TYPE_NONE = 0,
    TYPE_TMPFS
} MOUNT_TYPE;

static char *mounts[ MAX_BIND_DIRS ];
static MOUNT_TYPE mount_types[ MAX_BIND_DIRS ];
static char *mount_tmpfs_options;
static int mounts_count = 0;
static char *base = NULL;
static char *base_job_step = NULL;
static int _tmpdir_rm_count;

#define mfree( x ) { if ( x ) { free( x ); } x = NULL; }

/* vmprintf - stdargs dynamically allocate a formatted string */
static char *vmprintf( const char *fmt, va_list ap ) {
    va_list aq;
    va_copy( aq, ap );
    size_t len = vsnprintf( NULL, 0, fmt, ap ) + 1;
    char *data = (char*)malloc( sizeof( char ) * len );
    vsnprintf( data, len, fmt, aq );
    return( data );
}

/* mprintf - dynamically allocate a formatted string */
static char *mprintf( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *data = vmprintf( fmt, ap );
    va_end( ap );
    return( data );
}

/* remove any trailing slashes from a string
 * don't check the first charcater of the string
 * (thus ensuring '/' isn't affected
 */
static void remove_trailing_slash( char *path ) {
    size_t len = strlen( path );
    while (( len > 1 )&&( path[ len - 1 ] == '/' )) {
        path[ -- len ] = '\0';
    }
}

/* create a directory also setting the uid and gid */
static int create_tree_mkdir( const char *path, uid_t uid, gid_t gid ) {

    if ( mkdir( path, 0700 ) < 0 ) {
        slurm_error("%s: mkdir( %s, 0700 ) = %m", plugin_name, path );
        return( -1 );
    } else if ( chown( path, uid, gid )) {
        slurm_error("%s: chown(%s,%u,%u): %m", plugin_name, path, uid, gid);
        return( -1 );
    } else {
        return( 0 );
    }

}

/*
 * emulate a 'mkdir -p' but only for the path elements after "base"
 *
 * ie: create_tree( "/a", "/a/b/c", ... ) will
 *    mkdir /a/b
 *    mkdir /a/b/c
 */
static int create_tree( const char *base, const char *path, uid_t uid, gid_t gid ) {

    size_t base_len = strlen( base );
    size_t path_len = strlen( path );

    char *data = (char*)malloc( sizeof( char ) * ( base_len + path_len + 1 ));

    strcpy( data, base );
    data[ base_len ] = '/';

    for ( int ctr = 1 ; path[ ctr ] ; ctr ++ ) {

        if ( path[ ctr ] == '/' ) {
            data[ ctr + base_len ] = '\0';
            /* intermediate directories (if any) are owned by root */
            if ( create_tree_mkdir( data, 0, 0 ) < 0 ) {
                free( data );
                return( -1 );
            }
        }

        data[ ctr + base_len ] = path[ ctr ];

    }
    data[ base_len + path_len ] = '\0';
    /* final director is owned by the user */
    create_tree_mkdir( data, uid, gid );
    free( data );

    return( 0 );
}

/*
 * callback for nftw - file tree walk
 */
static int rmFiles(const char *pathname, const struct stat *sbuf, int type, struct FTW *ftwb) {

    /* use 'remove' so that both directories and files can be handled */
    if ( remove(pathname ) < 0) {
        slurm_error("%s: remove %s: %m", plugin_name, pathname);
        return -1;
    } else {
        _tmpdir_rm_count += 1;
        return 0;
    }
}

/*
 * stringAppendNumber converts a number to a char* and appends
 *   it to an existing malloc'd char*
 */

static void stringAppendNumber( char **ptr, int *len, int number ) {

    /* calculate the length of the char * required */

    int chlen;
    int tmp = number;

    for ( chlen = 0 ; tmp > 0 ; chlen ++, tmp /= 10 ) {
        /* spin */
    }

    /* add room onto the end of the existing string for the number */
    *ptr = (char*)realloc( *ptr, sizeof( char ) * ( *len + chlen + 1 ) );

    /* terminate the string */
    (*ptr)[ *len + chlen ] = '\0';

    /* copy the digits of the number into the string */
    for ( int ctr = chlen - 1 ; ctr >= 0 ; ctr -- ) {
        (*ptr)[ *len + ctr ] = ( number % 10 ) + '0';
        number /= 10;
    }

    *len += chlen;

}

/*
 * parse a tmpfs mount point, interpolating variables as required
 *
 *  <uid> = user ID (integer)
 */
static char *parseMountPath( const char *value ) {

    char *path = (char*)malloc( sizeof( char ) );
    *path = '\0';
    int len = 0;

    while ( *value ) {

        if ( strncmp( value, "<uid>", 5 ) == 0 ) {
            /* append the user ID */
            stringAppendNumber( &path, &len, uid );
            value += 5;
        } else {
            /* copy a regular character */
            path[ len ++ ] = *value;
            path = (char*)realloc( path, sizeof( char ) * ( len + 1 ) );
            path[ len ] = '\0';
            value ++;
        }

    }

    return( path );
}

/*
 * ignore the "external" step
 */
bool isRealStep( spank_t sp ) {

    if ( spank_remote( sp ) < 1 ) {
        /* In remote context, the plugin is loaded by slurmstepd.
         * (i.e. the "remote" part of a parallel job). */
        return( false );
    } else if ( spank_get_item(sp, S_JOB_STEPID, &stepid) != ESPANK_SUCCESS) {
        /* if unable to obtain stepid then don't handle */
        slurm_error("%s: spank_get_item( S_JOB_STEPID ) failed", plugin_name );
        return( false );
    } else if ( stepid == SLURM_BATCH_SCRIPT ) {
        /* handle the 'batch' step . . */
        return( true );
    } else if ( stepid == SLURM_INTERACTIVE_STEP ) {
        /* handle the 'interactive' step . . */
        return( true );
    } else if ( stepid <= SLURM_MAX_NORMAL_STEP_ID ) {
        /* handle normal step */
        return( true );
    } else {
        /* else do not handle */
        return( false );
    }
}

/*
 * return pointer to characters in a string if "match_text" is matched.
 * return NULL if "match_text" is not found at the start of the string.
 */
static const char *matches_prefix( const char *match_text, const char *text ) {

    size_t len = strlen( match_text );

    if ( strncmp( match_text, text, len ) == 0 ) {
        return( &text[ len ] );
    } else {
        return( NULL );
    }
}

/*
 *  Called from both srun and slurmd.
 */
int slurm_spank_init(spank_t sp, int ac, char **av)
{
    if ( ! isRealStep( sp ) ) {
        return( 0 );
    }
#ifdef DEBUG
    slurm_info("slurm_spank_init( context = %d )" , spank_context() );
#endif

    if (spank_get_item(sp, S_JOB_UID, &uid) != ESPANK_SUCCESS) {
        slurm_error("Unable to get job's user id");
        return( -1 );
    }

    if (spank_get_item(sp, S_JOB_GID, &gid) != ESPANK_SUCCESS) {
        slurm_debug("%s: Unable to get job's group id", plugin_name );
        return( -1 );
    }

    /* initialise mount definitions */

    mount_tmpfs_options = NULL;

    for ( size_t index = 0 ; index < MAX_BIND_DIRS ; index ++ ) {
        mounts[ index ] = NULL;
        mount_types[ index ] = TYPE_NONE;
    }

    /* configuration options:
     *
     *   base=<path to storage>
     *
     *   mount=<path to be mounted>
     */

    /* for each argument in the plugstack.conf */
    for ( int i = 0 ; i < ac ; i++ ) {

        const char *value;

        if (( value = matches_prefix("base=", av[ i ] )) != NULL ) {

            if ( *value != '/' ) {
                slurm_error("%s: base path must start with a '/': '%s'", plugin_name, value );
                return( -1 );
            } else {
                base = strdup( value );
                remove_trailing_slash( base );
            }

        } else if (( value = matches_prefix( "mount=", av[ i ] )) != NULL ) {

            if ( mounts_count >= MAX_BIND_DIRS ) {
                slurm_error("%s: maximum of %d mounts permitted", plugin_name, mounts_count );
                return( -1 );
            } else if ( *value != '/' ) {
                slurm_error("%s: mount path must start with a '/': '%s'", plugin_name, value );
                return( -1 );
            } else {
                mounts[ mounts_count ] = strdup( value );
                mount_types[ mounts_count ] = TYPE_NONE;
                remove_trailing_slash( mounts[ mounts_count ] );
                mounts_count ++;
            }

        } else if (( value = matches_prefix( "tmpfs=", av[ i ] )) != NULL ) {

            if ( mounts_count >= MAX_BIND_DIRS ) {
                slurm_error("%s: maximum of %d mounts permitted", plugin_name, mounts_count );
                return( -1 );
            } else if ( *value != '/' ) {
                slurm_error("%s: mount path must start with a '/': '%s'", plugin_name, value );
                return( -1 );
            } else {
                mounts[ mounts_count ] = parseMountPath( value );
                mount_types[ mounts_count ] = TYPE_TMPFS;
                remove_trailing_slash( mounts[ mounts_count ] );
                mounts_count ++;
            }

        } else if (( value = matches_prefix( "tmpfs/options=", av[ i ] )) != NULL ) {

            mfree( mount_tmpfs_options );
            mount_tmpfs_options = strdup( value );

        } else {
            slurm_info("%s: mount definition '%s' ignored (no match found for the type)",
                    plugin_name, av[i] );

        }

    }

    if ( base == NULL ) {
        slurm_error("%s: base=<path> must be defined", plugin_name );
        return( -1 );
    } else if ( mounts_count == 0 ) {
        slurm_error("%s: at least one mount=<path> must be defined", plugin_name );
        return( -1 );
    }

    slurm_info("%s: mount base is %s with %d bind mounts", plugin_name, base, mounts_count );

    struct stat buf;

    if ( stat( base, &buf ) < 0 ) {
        slurm_error("%s: failed to stat %s: %s", plugin_name, base, strerror( errno ));
        return( -1 );
    } else if ( ! S_ISDIR( buf.st_mode ) ) {
        slurm_error("%s: %s must be a directory", plugin_name, base );
        return( -1 );
    }

    /* fetch the job information */

    if (spank_get_item(sp, S_JOB_ID, &jobid) != ESPANK_SUCCESS) {
        slurm_error("%s: Failed to get jobid from SLURM", plugin_name);
        return( -1 );
    }

    if ( spank_get_item(sp, S_SLURM_RESTART_COUNT, &restartcount ) != ESPANK_SUCCESS) {
        slurm_debug( "%s: Unable to get job's restart count", plugin_name );
        return( -1 );
    }

    /* create the directory for the job / step / restart data */

    if ( stepid == SLURM_BATCH_SCRIPT ) {
        base_job_step = mprintf( "%s/%u.batch.%u", base, jobid, restartcount );
    } else if ( stepid == SLURM_INTERACTIVE_STEP ) {
        base_job_step = mprintf( "%s/%u.interactive.%u", base, jobid, restartcount );
    } else {
        base_job_step = mprintf( "%s/%u.%u.%u", base, jobid, stepid, restartcount );
    }

    if ( mkdir( base_job_step, 0700 ) ) {
        slurm_error("%s: mkdir(\"%s\",0700): %m", plugin_name, base_job_step );
        return( -1 );
    }

    slurm_info("%s: root data store is at %s", plugin_name, base_job_step );

    if (mount("", "/", "dontcare", MS_REC | MS_SHARED, "")) {
        slurm_error("%s: failed to 'mount --make-rshared /': %m", plugin_name );
        return( -1 );
    }
    // Create our own namespace
    if (unshare(CLONE_NEWNS)) {
        slurm_error("%s: failed to unshare mounts: %m", plugin_name );
        return( -1 );
    }
    // Make / slave (same as mount --make-rslave /)
    if (mount("", "/", "dontcare", MS_REC | MS_SLAVE, "")) {
        slurm_error("%s: failed to 'mount --make-rslave /': %m", plugin_name );
        return( -1 );
    }

    for ( int i = 0 ; i < mounts_count ; i ++ ) {

        if ( mount_types[ i ] == TYPE_TMPFS ) {
            /* tmpfs is handled differently */

            struct stat buf;

            if ( stat( mounts[ i ], &buf ) < 0 ) {
                if ( errno == ENOENT ) {
                    if ( create_tree_mkdir( mounts[ i ], uid, gid ) < 0 ) {
                        slurm_error("%s: TMPFS %s mktree failed",
                                plugin_name, mounts[ i ] );
                        continue;
                    }
                } else {
                    slurm_error("%s: failed to stat TMPFS directory %s: %s",
                            plugin_name, mounts[ i ], strerror( errno ));
                    continue;
                }
            } else if ( ! S_ISDIR( buf.st_mode ) ) {
                slurm_error("%s: TMPFS mount %s exists and is not a directory",
                            plugin_name, mounts[ i ] );
                continue;
            }

            char *opts = mprintf( "mode=750,uid=%d%s%s", uid,
                    ( mount_tmpfs_options ? "," : "" ),
                    ( mount_tmpfs_options ? mount_tmpfs_options : "" ) );

            if ( mount( "tmpfs", mounts[ i ], "tmpfs", 0, opts ) < 0 ) {
                slurm_error("%s: failed to mount TMPFS dir=%s: %m", plugin_name, mounts[ i ] );
                free( opts );
                return( -1 );
            }
            free( opts );

            slurm_info("%s: mounted TMPFS at %s", plugin_name, mounts[ i ] );

        } else {

            if ( create_tree( base_job_step, mounts[ i ], uid, gid ) < 0 ) {
                return( -1 );
            }

            char *path = mprintf("%s%s", base_job_step, mounts[ i ] );

            if ( mount( path, mounts[ i ], "none", MS_BIND, NULL ) < 0 ) {
                slurm_error("%s: failed to mount path=%s dir=%s: %m", plugin_name, path, mounts[ i ] );
                free( path );
                return( -1 );
            }

            slurm_info("%s: mounted %s at %s", plugin_name, path, mounts[ i ] );
            free( path );

        }

    }

    return( 0 );
}

int slurm_spank_exit(spank_t sp, int ac, char **av)
{
    if ( ! isRealStep( sp ) ) {
        return( 0 );
    }
#ifdef DEBUG
    slurm_info("slurm_spank_exit( context = %d )" , spank_context() );
#endif

    /* release memory */
    mfree( mount_tmpfs_options );

    if ( base_job_step ) {

        struct timeval begin_time, end_time;

        _tmpdir_rm_count = 0;

        gettimeofday( &begin_time, NULL );

        /* file tree walk using:
         *      post-order traversal (FTW_DEPTH)
         *      do not cross mount points (FTW_MOUNT)
         *      do not follow symbolic links (FTW_PHYS)
         */
        if (nftw( base_job_step, rmFiles, 10, FTW_DEPTH|FTW_MOUNT|FTW_PHYS) < 0) {
            slurm_error("%s: %s nftw error %m", plugin_name, base_job_step );
        }

        gettimeofday( &end_time, NULL );

        end_time.tv_sec -= begin_time.tv_sec;
        if ( end_time.tv_usec < begin_time.tv_usec ) {
            end_time.tv_usec += ( 1000000 - begin_time.tv_usec );
            end_time.tv_sec += 1;
        } else {
            end_time.tv_usec -= begin_time.tv_usec;
        }

        slurm_info( "%s: removed %s (%d files) in %ld.%06ld seconds", plugin_name, base_job_step,
                _tmpdir_rm_count, end_time.tv_sec, end_time.tv_usec );

    }

    free( base_job_step );

    return( 0 );
}

#if DEBUG_2
int slurm_spank_job_prolog(spank_t sp, int ac, char **av) {
    if ( ! isRealStep( sp ) ) {
        return( 0 );
    }
    slurm_info("slurm_spank_job_prolog( context = %d )" , spank_context() );
    return( 0 );
}
#endif

#if DEBUG_2
int slurm_spank_job_epilog(spank_t sp, int ac, char **av)
{
    if ( ! isRealStep( sp ) ) {
        return( 0 );
    }
    slurm_info("slurm_spank_job_epilog( context = %d )" , spank_context() );
    return 0;
}
#endif

#ifdef DEBUG_2
int slurm_spank_init_post_opt(spank_t sp, int ac, char **av)
{
    if ( ! isRealStep( sp ) ) {
        return( 0 );
    }
    slurm_info("slurm_spank_init_post_opt( context = %d )" , spank_context() );
    return _tmpdir_bind(sp, ac, av);
}
#endif

/* END OF FILE */
